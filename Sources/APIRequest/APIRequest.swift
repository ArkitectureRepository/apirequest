import Foundation
import Combine

public enum APIMethod: String {
    case GET, POST, UPDATE, DELETE, PUT
}

public protocol APIRequest {
    
    associatedtype Response: Codable
    
    typealias APIRequestResponse = Result<Response,APIError>
    
    var method: APIMethod { get }
    var baseURL: String { get }
    var path: String { get }
    var parameters: [String: String] { get }
    var body: Any? { get }
    var headers: [String: String] { get }
    var timeOut: TimeInterval { get }
    
    func makeRequest() async -> APIRequestResponse
}

//MARK: - Default values
public extension APIRequest {
    
    var headers: [String: String] { ["Content-Type":"application/json"] }
    var parameters: [String: String] { return [:] }
    var body: Any? { return nil }
    var timeOut: TimeInterval { return 60 }
    
    @discardableResult
    func makeRequest() async -> APIRequestResponse {
        await APISession().request(self)
    }
}

//MARK: - Generate request
extension APIRequest {
    var request: URLRequest {
        
        guard var url = URL(string: baseURL) else {
            fatalError("Impossible to form base URL")
        }
        
        var path = path
        if !path.isEmpty {
            if path.first != "/" {
                path = "/" + path
            }
            url.appendPathComponent(path)
        }
        
        guard var components = URLComponents(url: url, resolvingAgainstBaseURL: false) else {
            fatalError("Impossible to create URLComponent from \(url)")
        }
        
        if !parameters.isEmpty {
            components.queryItems = parameters.map{ URLQueryItem(name: $0, value: $1) }
        }
        
        guard let finalUrl = components.url else {
            fatalError("Unable to retrieve final URL")
        }
        
        var request = URLRequest(url: finalUrl)
        request.httpMethod = method.rawValue
        if let body = self.body {
            request.httpBody = try? JSONSerialization.data(withJSONObject: body, options: [])
        }
        request.allHTTPHeaderFields = self.headers
        request.timeoutInterval = timeOut
        
        return request
        
    }
}
