// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "APIRequest",
    platforms: [.iOS(.v15)],
    products: [
        .library(
            name: "APIRequest",
            targets: ["APIRequest"]),
    ],
    dependencies: [],
    targets: [
        
        .target(
            name: "APIRequest",
            dependencies: []),
        
        .testTarget(
            name: "APIRequestTests",
            dependencies: ["APIRequest"]),
    ]
)
