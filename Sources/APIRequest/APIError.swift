import Foundation

public struct APIError: Error {
    public var url: String
    public var statusCode: Int
    public var data: Data? = nil
    public var message: String
}

//MARK: - Default API errors
public extension APIError {
    static func network(_ url: String) -> APIError {
        .init(url: url, statusCode: -1, message: "Network connection fails")
    }
    
    static func parseData(_ url: String) -> APIError {
        .init(url: url, statusCode: -2, message: "Parse data error")
    }
    
    static func unknow(_ url: String) -> APIError {
        .init(url: url, statusCode: -3, message: "Unknow error")
    }
    
    static func downloadFolder(_ url: String) -> APIError {
        .init(url: url, statusCode: -4, message: "Download folder error")
    }
    
    static func mock(_ url: String) -> APIError {
        .init(url: url, statusCode: -5, message: "Error getting mocked file")
    }
}

//MARK: - Description
extension APIError: CustomStringConvertible {
    public var description: String {
        return #"""
        Url: \#(url)
        Status code: \#(statusCode)
        Message: \#(message)
        """#
    }
}
