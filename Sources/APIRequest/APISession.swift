import Foundation
import Combine

class APISession {
    
    var publisher: URLSession.DataTaskPublisher?
    var cancellable: AnyCancellable?
    
    func request<Request: APIRequest>(_ request: Request) async -> Request.APIRequestResponse {
        await withCheckedContinuation({ continuation in
            let request = request.request
            let urlStr = request.url!.absoluteString
            self.publisher = URLSession.shared.dataTaskPublisher(for: request)
            self.cancellable = self.publisher?.sink(receiveCompletion: { _ in }, receiveValue: { data, response in
                guard let response = response as? HTTPURLResponse else {
                    continuation.resume(returning: .failure(.unknow(urlStr)))
                    return
                }
                
                let statusCode = response.statusCode
                switch statusCode {
                case 200..<300:
                    if Request.Response.self == Data.self, let data = data as? Request.Response {
                        continuation.resume(returning: .success(data))
                        return
                    }
                    
                    do {
                        let model = try JSONDecoder().decode(Request.Response.self, from: data)
                        continuation.resume(returning: .success(model))
                        return
                    } catch {
                        continuation.resume(returning: .failure(.parseData(urlStr)))
                        return
                    }
                default:
                    let error = APIError.init(url: urlStr, statusCode: statusCode, data: data, message: "Service error")
                    continuation.resume(returning: .failure(error))
                }
            })
        })
    }
    
}
