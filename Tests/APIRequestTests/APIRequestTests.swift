import XCTest
@testable import APIRequest

final class APIRequestTests: XCTestCase {
    
    struct APIResponseModel: Codable {
        let request_hash: String
    }
    
    struct APITestGet: APIRequest {
        var method: APIMethod { .GET }
        
        var baseURL: String { "https://api.jikan.moe" }
        
        var path: String { "v3/anime/1/episodes/2" }
        
        typealias Response = APIResponseModel
    }
    
    func testGet() throws {
        let expectation = XCTestExpectation(description: "Get API response")
        
        Task {
            let response = await APITestGet().makeRequest()
            
            switch response {
            case .success(let model):
                XCTAssert(!model.request_hash.isEmpty, model.request_hash)
                expectation.fulfill()
            case .failure:
                XCTFail()
            }
        }
        
        wait(for: [expectation], timeout: 5.0)
    }
}
